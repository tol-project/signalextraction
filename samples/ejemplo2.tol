//////////////////////////////////////////////////////////////////////////////
// Ejemplo II: 
//////////////////////////////////////////////////////////////////////////////

#Require MMS;

Real PutRandomSeed(23);
  
Date begin = y2000m01;
Date end = y2005m12;

// Especificamos Arima  
Set arima = GetArimaFromLabel("P1_12DIF0_1AR1_0MA0_12");  
 
Real P = Degree(ARIMAGetAR(arima));
Real D = Degree(ARIMAGetDIF(arima));
Real Q = Degree(ARIMAGetMA(arima));

Real initLength = 100*Max(P+D, Q);
Date begin_ = Succ(begin, Mensual, -initLength);
Serie r0 = SubSer(Gaussian(0, 0.2, Mensual), begin_, end);
Serie n0 = DifEq(ARIMAGetMA(arima)/ARIMAGetARI(arima), r0);
Serie residuals = SubSer(r0, begin, end);
Serie noise = SubSer(n0, begin, end);
    

// Creamos el dataSet y las variables
MMS::@DataSet DS.PjA = MMS::Container::ReplaceDataSet([[
  Text _.name = "Ejemplo_A";
  Text _.description = "Data Set para el Ejemplo_A"
]]);

Anything DS.PjA::ReplaceVariable([[
  Text _.name = "VariableA1";
  Text _.source = "Ejemplo_A";
  Text _.expression = "Serie noise"
]]);

// Creamos el modelo y el submodelo
MMS::@Model model = MMS::Container::ReplaceModel([[
  Text _.name = "ModelA";
  Text _.version = "1";
  Set _.dataSets = [[ "Ejemplo_A" ]]
]]);
  
MMS::@Submodel submodel = model::CreateSubmodel([[
  Text _.name = "OutputA";
  NameBlock _.output = [[
    Text _.name = "VariableA1";
    Text _.variable = "VariableA1"
  ]];
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P1_12DIF0_1AR1_0MA0_12"; 
    Real _.sigma = 1
  ]]
]]); 

// Creamos la estimaci�n MLE
Real estimNum = MMS::Container::FindEstimation([["ModelA","1","Estimate"]]);
Real If(estimNum, MMS::Container::RemoveEstimation(estimNum));

MMS::@Estimation estimation = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimate";
  MMS::@Model _.model = MMS::Container::GetModel([["ModelA","1"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces =  True
  ]]
]]);

// Ejecuci�n de la estimaci�n
Real estimation::Execute(?);

// Extraemos el noise y el arima
Set arima_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetARIMA(?);
Serie noise_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetNoise(?);

Serie noiseB = SignalExtraction::ForecastingNoiseB(noise_, arima_, 200);
Serie noiseF = SignalExtraction::ForecastingNoiseF(noise_, arima_, 200);

//////////////////////////////////////////////////////////////////////////////
// Periodo             AR                MA                DIF
//  1.0	    1+0.0201521069002411*B	  1	            1
//  12.0	        1  	1+0.148126084930786*B^12   1-B^12

// Descomponemos en factores
Polyn tetha = SetProd(EvalSet(arima_, Polyn (Set se) { se["ma"] }));

Polyn ar1 = arima_[1]["ar"];
Polyn dif12 = arima_[2]["dif"];
Polyn phi1 = ar1;
Polyn phi2 = (1-B);
Polyn phi3 = Quotient(dif12/(1-B));
Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima_, noise_));


// Grado del numerador: 12
// Grado del denominador: 13


// 1� Utilizamos la funci�n MXSBurman: DA ERROR -> no puede dividir dos polinomios
Set se_B = SignalExtraction::MSXBurman( tetha, SetOfPolyn(phi1, phi2, phi3),
          noise_, noiseB, noiseF);

// 2� 
Set se_New3 = SignalExtraction::signals( tetha, SetOfPolyn(phi1, phi2, phi3),
          noise_, noiseB, noiseF, v);

Set se_New2 = SignalExtraction::signals( tetha, SetOfPolyn(phi1*phi2, phi3),
          noise_, noiseB, noiseF, v);

/////////////////////////////////////////////////////////////////////////////

// Probamos otro ejemplo donde salgan par�metros el ar mas alto
// Especificamos Arima  
Set arima_2 = GetArimaFromLabel("P1_12DIF1_1AR0_12MA1_0");  
 
Real P2 = Degree(ARIMAGetAR(arima_2));
Real D2 = Degree(ARIMAGetDIF(arima_2));
Real Q2 = Degree(ARIMAGetMA(arima_2));

Real initLength_2 = 100*Max(P2+D2, Q2);
Date begin_2 = Succ(begin, Mensual, -initLength_2);
Serie r0_2 = SubSer(Gaussian(0, 0.2, Mensual), begin_2, end);
Serie n0_2 = DifEq(ARIMAGetMA(arima_2)/ARIMAGetARI(arima_2), r0_2);
Serie residuals_2 = SubSer(r0_2, begin, end);
Serie noise_2 = SubSer(n0_2, begin, end);
  
// Creamos el dataSet y las variables
MMS::@DataSet DS.PjA_2 = MMS::Container::ReplaceDataSet([[
  Text _.name = "Ejemplo_A_2";
  Text _.description = "Data Set para el Ejemplo_A_2"
]]);

Anything DS.PjA_2::ReplaceVariable([[
  Text _.name = "VariableA1_2";
  Text _.source = "Ejemplo_A_2";
  Text _.expression = "Serie noise_2"
]]);

// Creamos el modelo y el submodelo
MMS::@Model model_2 = MMS::Container::ReplaceModel([[
  Text _.name = "ModelA_2";
  Text _.version = "1";
  Set _.dataSets = [[ "Ejemplo_A_2" ]]
]]);
  
MMS::@Submodel submodel_2 = model_2::CreateSubmodel([[
  Text _.name = "OutputA_2";
  NameBlock _.output = [[
    Text _.name = "VariableA1_2";
    Text _.variable = "VariableA1_2"
  ]];
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P1_12DIF0_1AR1_0MA0_12"; 
    Real _.sigma = 1
  ]]
]]); 

// Creamos la estimaci�n MLE
Real estimNum_2 = MMS::Container::FindEstimation([["ModelA_2","1","Estimate_2"]]);
Real If(estimNum_2, MMS::Container::RemoveEstimation(estimNum_2));

MMS::@Estimation estimation_2 = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimate_2";
  MMS::@Model _.model = MMS::Container::GetModel([["ModelA_2","1"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces =  True
  ]]
]]);

// Ejecuci�n de la estimaci�n
Real estimation_2::Execute(?);


// Extraemos el noise y el arima
Set arima2_ = estimation_2::GetModel.Results(?)::GetSubmodel(1)::GetARIMA(?);
Serie noise2_ = estimation_2::GetModel.Results(?)::GetSubmodel(1)::GetNoise(?);

//////////////////////////////////////////////////////////////////////////////
// Periodo              AR                  MA                DIF
// 1.0	       1-0.440443373318404*B	     1	                1
// 12.0            	1	1-0.0751535833516761*B^12	1-B^12


Serie noiseB2 = SignalExtraction::ForecastingNoiseB(noise2_, arima2_, 200);
Serie noiseF2 = SignalExtraction::ForecastingNoiseF(noise2_, arima2_, 200);


// Descomponemos en factores
Polyn tetha2 = SetProd(EvalSet(arima2_, Polyn (Set se) { se["ma"] }));

Polyn ar1_ = arima2_[1]["ar"];
Polyn dif12_ = arima2_[2]["dif"];
Polyn phi1_ = ar1_;
Polyn phi2_ = (1-B);
Polyn phi3_ = Quotient(dif12_/(1-B));
Real v_ = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima2_, noise2_));


// Grado del numerador: 12
// Grado del denominador: 13


// 1� Utilizamos la funci�n MXSBurman: DA ERROR -> 
//    no puede dividir dos polinomios
Set se_B2 = SignalExtraction::MSXBurman(tetha2, 
    SetOfPolyn(phi1_*phi2_, phi3_), noise2_, noiseB2, noiseF2);

// 2� 
Set se_New_2 = SignalExtraction::signals(tetha2,
    SetOfPolyn(phi1_*phi2_, phi3_), noise2_, noiseB2, noiseF2, v_);
