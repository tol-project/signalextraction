//////////////////////////////////////////////////////////////////////////////
// Ejemplo III:  Residuos ALTOS
//               Mala Extracción
//////////////////////////////////////////////////////////////////////////////

#Require MMS;

Real PutRandomSeed(23);

Date begin = y2008m01;
Date end = y2009m12;

// Arima
Text labelarima = "P1_30DIF0_1AR1_0MA0_30";

Set arima = GetArimaFromLabel(labelarima);

Real P = Degree(ARIMAGetAR(arima)); 
Real D = Degree(ARIMAGetDIF(arima)); 
Real Q = Degree(ARIMAGetMA(arima));

Real ini = 100*Max(P+D, Q);
Date begin_ = Succ(begin, Diario, -ini);  
Serie r0 = SubSer(Gaussian(0, 0.2, Diario), begin_, end);
Serie n0 = DifEq(ARIMAGetMA(arima)/ARIMAGetARI(arima), r0);

Serie residuals = SubSer(r0, begin, end);
Serie noise = SubSer(n0, begin, end);

// Creamos el dataSet y las variables
MMS::@DataSet DS.PjB = MMS::Container::ReplaceDataSet([[
  Text _.name = "Ejemplo_B";
  Text _.description = "Data Set para el Ejemplo_B"
]]);

Anything DS.PjB::ReplaceVariable([[
  Text _.name = "VariableB1";
  Text _.source = "Ejemplo_B";
  Text _.expression = "Serie noise"
]]);

// Creamos el modelo y el submodelo
MMS::@Model modelB = MMS::Container::ReplaceModel([[
  Text _.name = "ModelB";
  Text _.version = "1";
  Set _.dataSets = [[ "Ejemplo_B" ]]
]]);
  
MMS::@Submodel submodelB = modelB::CreateSubmodel([[
  Text _.name = "OutputB";
  NameBlock _.output = [[
    Text _.name = "VariableB1";
    Text _.variable = "VariableB1"
  ]];
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P30DIF1AR0MA30"; 
    Real _.sigma = 1
  ]]
]]); 

// Creamos la estimación MLE
Real estimNumB = MMS::Container::FindEstimation([["ModelB","1","Estimate"]]);
Real If(estimNumB, MMS::Container::RemoveEstimation(estimNumB));

MMS::@Estimation estimationB = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimate";
  MMS::@Model _.model = MMS::Container::GetModel([["ModelB","1"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces =  True
  ]]
]]);

// Ejecución de la estimación
Real estimationB::Execute(?);

// Noise y ARIMA
Serie noise_ = estimationB::GetModel.Results(?)::GetSubmodel(1)::GetNoise(?); 
Set arima_ = estimationB::GetModel.Results(?)::GetSubmodel(1)::GetARIMA(?);

//////////////////////////////////////////////////////////////////////////////
// Periodo         AR                 MA                  DIF
//  30.0           1      1+0.104389056316037*B^30	1-B^30

// Factorizamos los polinomios
Polyn tetha = arima_[1]["ma"];
Polyn dif1 = 1-B;
Polyn dif30 = arima_[1]["dif"];
Polyn phi2 = Quotient(dif30/(1-B));

// Forecasting
Real n = CountS(noise_);
Serie noiseB = SignalExtraction::ForecastingNoiseB(noise_, arima_, n/2);
Serie noiseF = SignalExtraction::ForecastingNoiseF(noise_, arima_, n/2);

// MSXBurman
Set se_B = SignalExtraction::
  MSXBurman(tetha, SetOfPolyn(dif1, phi2), noise_, noiseB, noiseF);

Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima_, noise_));
Set se_New = SignalExtraction::
  signals(tetha, SetOfPolyn(dif1, phi2), noise_, noiseB, noiseF, v);






