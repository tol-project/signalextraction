//////////////////////////////////////////////////////////////////////////////
// Polinomios grandes
// Periodo 365 

#Require MMS;

Real PutRandomSeed(2143);
  
Date begin = y2008m01;
Date end = y2009m12;

// Especificamos Arima
Text labelarima = "P1_365DIF0_1AR1_0MA0_365"; 
  
Set arima = GetArimaFromLabel(labelarima);  //ARIMA.FromLabel(labelarima);
 
Real P = Degree(ARIMAGetAR(arima));
Real D = Degree(ARIMAGetDIF(arima));
Real Q = Degree(ARIMAGetMA(arima));

Real ini = 100*Max(P+D, Q);
Date begin_ = Succ(begin, Diario, -ini);
Serie r0 = SubSer(Gaussian(0, 0.2, Diario), begin_, end);
Serie n0 = DifEq(ARIMAGetMA(arima)/ARIMAGetARI(arima), r0);

Serie residuals = SubSer(r0, begin, end);
Serie noise = SubSer(n0, begin, end);

// Creamos el dataSet y las variables
MMS::@DataSet DS.PjA = MMS::Container::ReplaceDataSet([[
  Text _.name = "Ejemplo_A";
  Text _.description = "Data Set para el Ejemplo_A"
]]);

Anything DS.PjA::ReplaceVariable([[
  Text _.name = "VariableA1";
  Text _.source = "Ejemplo_A";
  Text _.expression = "Serie noise"
]]);

// Creamos el modelo y el submodelo
MMS::@Model model = MMS::Container::ReplaceModel([[
  Text _.name = "ModelA";
  Text _.version = "1";
  Set _.dataSets = [[ "Ejemplo_A" ]]
]]);
  
MMS::@Submodel submodel = model::CreateSubmodel([[
  Text _.name = "OutputA";
  NameBlock _.output = [[
    Text _.name = "VariableA1";
    Text _.variable = "VariableA1"
  ]];
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P1_365DIF1_0AR0_365MA0_0"; 
    Real _.sigma = 1
  ]]
]]); 

// Creamos la estimación MLE
MMS::@Estimation estimation = MMS::Container::ReplaceEstimation([[
  Text _.name = "Estimate";
  MMS::@Model _.model = MMS::Container::GetModel([["ModelA","1"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces =  True
  ]]
]]);

// Ejecución de la estimación
Real estimation::Execute(?);

// Noise y ARIMA:
Serie noise_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetNoise(?);
Set arima_ = estimation::GetModel.Results(?)::GetSubmodel(1)::GetARIMA(?);


//////////////////////////////////////////////////////////////////////////////
//   Periodo           AR               MA          DIF
//     1.0              1            1-0.999*B	    1-B
//    365.0	1-0.945*B^365	         1	     1

Real n = CountS(noise_);
Serie noiseB = SignalExtraction::ForecastingNoiseB(noise_, arima_, n/2);
Serie noiseF = SignalExtraction::ForecastingNoiseF(noise_, arima_, n/2);

Polyn tetha = SetProd(EvalSet(arima_, Polyn (Set se) {se["ma"]}));
Polyn fi1 = arima_[2]["ar"];
Polyn dif1 = arima_[1]["dif"];
Set d = SignalExtraction::STFactorization(fi1);

Real v = VarS(SignalExtraction::ARIMAAlmagroEval_Residuals(arima_, noise_));
Set se = SignalExtraction::
  signals(tetha, SetOfPolyn(dif1*d[1], d[2]), noise_, noiseB, noiseF, v);

// No puede hacer la primera división, la función CV del denominador es muy pesada




