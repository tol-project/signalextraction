#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass extarticle
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package auto
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language french
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Bayesian Signal Extraction
\end_layout

\begin_layout Author
Víctor de Buen Remiro
\end_layout

\begin_layout Author
©Bayes Decision
\end_layout

\begin_layout Address
vdebuen@bayesforecast.com
\end_layout

\begin_layout Date
2015-03
\end_layout

\begin_layout Abstract
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Section
Descomposición estacional
\end_layout

\begin_layout Standard
Sea una serie temporal gaussiana estacional 
\begin_inset Formula $Z_{t}$
\end_inset

 que sigue un modelo ARIMA estacional de orden 
\begin_inset Formula $p$
\end_inset


\begin_inset Formula 
\begin{gather}
\left(1-B\right)^{d}\left(1-B^{p}\right)^{D}\phi\left(B\right)\Phi\left(B^{p}\right)Z_{t}=\theta\left(B\right)\varTheta\left(B^{p}\right)\varepsilon_{t}\\
d\geq0,\: D\geqq1\\
\varepsilon_{t}\sim N\left(0,\sigma^{2}I\right)\\
t=1,2,\ldots,m
\end{gather}

\end_inset


\end_layout

\begin_layout Standard
Se pretende encontrar una descomposición aditiva de 
\begin_inset Formula $Z_{t}$
\end_inset

 en tres componentes independientes: ciclo estacional 
\begin_inset Formula $S_{t}$
\end_inset

, tendencia 
\begin_inset Formula $T_{t}$
\end_inset

 y ruido estacionario 
\begin_inset Formula $N_{t}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{gather}
Z_{t}=S_{t}+T_{t}+N_{t}\\
\left(1+B+\cdots+B^{p-1}\right)^{D}\Phi_{S}\left(B^{p}\right)Z_{t}=\varTheta_{S}\left(B^{p}\right)\varepsilon_{S,t}\\
\left(1-B\right)^{d}\left(1+B\right)^{D}T_{t}=\theta_{T}\left(B\right)\varepsilon_{T,t}\\
\phi_{N}\left(B\right)N_{t}=\theta_{N}\left(B\right)\varepsilon_{N,t}\\
\varepsilon_{X,t}\sim N\left(0,\sigma_{X}^{2}I\right)\wedge X\in\left\{ S,T,N\right\} \\
Cov\left[\varepsilon_{S,t},\varepsilon_{T,t}\right]=Cov\left[\varepsilon_{S,t},\varepsilon_{N,t}\right]=Cov\left[\varepsilon_{T,t},\varepsilon_{N,t}\right]=0
\end{gather}

\end_inset


\end_layout

\begin_layout Standard
Las funciones generatrices de las autocovarianzas de los 4 procesos implicados
 son por cosntrucción
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\gamma_{Z}\left(B,F\right) & = & \sigma^{2} & \frac{\theta\left(B\right)\theta\left(F\right)\varTheta\left(B^{p}\right)\varTheta\left(F^{p}\right)}{\left(1-B\right)^{d}\left(1-F\right)^{d}\left(1-B^{p}\right)^{D}\left(1-F^{p}\right)^{D}\phi\left(B\right)\phi\left(F\right)\Phi\left(B^{p}\right)\Phi\left(F^{p}\right)}\\
\gamma_{S}\left(B,F\right) & = & \sigma_{S}^{2} & \frac{\theta_{T}\left(B\right)\theta_{T}\left(F\right)}{\left(1+B+\cdots+B^{p-1}\right)^{D}\left(1+F+\cdots+F^{p-1}\right)^{D}\Phi\left(B^{p}\right)\Phi\left(F^{p}\right)}\\
\gamma_{T}\left(B,F\right) & = & \sigma_{T}^{2} & \frac{\theta_{T}\left(B\right)\theta_{T}\left(F\right)}{\left(1-B\right)^{d}\left(1-F\right)^{d}\left(1+B\right)^{d}\left(1+F\right)^{d}}\\
\gamma_{N}\left(B,F\right) & = & \sigma_{N}^{2} & \frac{\theta_{N}\left(B\right)\theta_{N}\left(F\right)}{\phi_{N}\left(B\right)\phi_{N}\left(F\right)}
\end{align}

\end_inset


\end_layout

\begin_layout Standard
Dado que los procesos 
\begin_inset Formula $S_{t}$
\end_inset

, 
\begin_inset Formula $T_{t}$
\end_inset

 y 
\begin_inset Formula $N_{t}$
\end_inset

 son independientes las autocovarianzas del proceso original deben ser las
 sumas de las autovarianzas de sus componentes
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align}
\gamma_{Z}\left(B,F\right) & = & \gamma_{S}\left(B,F\right)+\gamma_{T}\left(B,F\right)+\gamma_{N}\left(B,F\right)
\end{align}

\end_inset


\end_layout

\end_body
\end_document
