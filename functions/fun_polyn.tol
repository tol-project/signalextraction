//////////////////////////////////////////////////////////////////////////////
// FILE   : fun_polyn.tol
// PURPOSE:
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// División de polinomios
Set PolDivision(Polyn a, Polyn b)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Degree(a);
  Real m = Degree(b);

  Case(
    b == Polyn 0, SetOfPolyn(UnknownPolyn, UnknownPolyn),
    EQ(m,0), SetOfPolyn(Quotient(a/b), 0),
    LT(n,m), SetOfPolyn(0, a),
    1, {
      Real d = n-m;
      Real k = m-1;

      VMatrix v  = Pol2VMat(ChangeBF((F^n)*a), n+1, 1);
      VMatrix w1 = Pol2VMat(ChangeBF((F^m)*b), n+1, d+1);
      VMatrix w2 = Eye(d+1, k+1, 0, 0);
      VMatrix w4 = Diag(k+1, 1);

      Matrix w = VMat2Mat(w1|(w2<<w4));
      Matrix z = GaussInverse(w)*VMat2Mat(v);
      
      If ( EQ(Rows(z), 0), {
        WriteLn("No es posible calcular la división de los polinomios:
                a-> "<<a<<"
                b-> "<<b, "E");
        Empty
      }, {
        Polyn q = MatPol(Tra(Reverse(SubRow(z, Range(1, d+1, 1))))); 
        Polyn r = MatPol(Tra(Reverse(SubRow(z, Range(d+2, n+1, 1)))));
        SetOfPolyn(q,r)
      })    
    }
  )
};

//////////////////////////////////////////////////////////////////////////////
// Evalua un ratio en un conjundo de valores (dominio)
Set DrawRatio(Ratio rat, Set domain)
//////////////////////////////////////////////////////////////////////////////
{
  Set pairSet = EvalSet(domain, Set(Real x){ 
    Real y = EvalRat(rat, x);
    SetOfReal(x, y)
  });
  pairSet
};

//////////////////////////////////////////////////////////////////////////////
// Espectro en la variable x=cosw
Set Spectrum(Polyn tetha, Polyn phi, Real sigma)
//////////////////////////////////////////////////////////////////////////////
{
  Ratio spectrum = 
   ChangeSymBFCosx(tetha*ChangeBF(tetha)*(2*sigma^2))/
   ChangeSymBFCosx(phi*ChangeBF(phi));

  Set DrawRatio(spectrum, Range(Cos(Pi), Cos(0), 0.001))

};

//////////////////////////////////////////////////////////////////////////////
Set Spectrum2(Polyn tethaBF, Polyn phi)
//////////////////////////////////////////////////////////////////////////////
{
  Ratio spectrum = 
   ChangeSymBFCosx(tethaBF)/
   ChangeSymBFCosx(phi*ChangeBF(phi));

  Set DrawRatio(spectrum, Range(Cos(Pi), Cos(0), 0.001))
};

//////////////////////////////////////////////////////////////////////////////
// polSet = SetOfPolyn(a,b,c);
// Devuelve los factores xa, xb, xc
// 1/ abc = xa*(1/a)+xb*(1/b)+xc*(1/c);
// Luego: 1 = xa*b*c+xb*a*c+xc*a*b;
Set InvPolExtGCD(Set polSet)
//////////////////////////////////////////////////////////////////////////////
{
  Real c = Card(polSet);
  Case(
    EQ(c, 0), Empty,
    EQ(c, 1), {
      Set factorSet = SetOfPolyn(1);
      factorSet
    },
    1, {
      Set extGCD    = PolExtendedGCD(polSet[1], polSet[2]);
      If ( EQ(Card(extGCD), 0), Empty, {
        Set factorSet = ReverseSet(extGCD[1]);
        Real k = 3;
        Real While(LE(k, Card(polSet)), {
          Polyn prodk_1 = SetProd(ExtractByIndex(polSet, Range(1, k-1, 1)));
          Set extGCDNew = PolExtendedGCD(polSet[k], prodk_1);
          Set factorSet := 
          EvalSet(factorSet, Polyn(Polyn factor){extGCDNew[1][1]*factor})<<
          SetOfPolyn(extGCDNew[1][2]);
          Real k := k +1 
        });
        factorSet
      })   
    }
  )
};


//////////////////////////////////////////////////////////////////////////////
Set PolExtGCD(Set polSet)
//////////////////////////////////////////////////////////////////////////////
{
  Real c = Card(polSet);
  Case(
    EQ(c, 0), Empty,
    EQ(c, 1), {
      Set factorSet = SetOfPolyn(1);
      Polyn gcd = polSet[1];
      SetOfAnything(factorSet, gcd)
    },
    1, {
      Set extGCD    = PolExtendedGCD(polSet[1], polSet[2]);
      Set factorSet = extGCD[1];
      Polyn gcd     = extGCD[2];
      Real k = 3;
      Real While(LE(k, Card(polSet)), {
        Set extGCDNew = PolExtendedGCD(gcd, polSet[k]);
        Set factorSet := 
        EvalSet(factorSet, Polyn(Polyn factor) {extGCDNew[1][1]*factor})
        <<SetOfPolyn(extGCDNew[1][2]);
        Polyn gcd := extGCDNew[2];
        Real k := k + 1 
      });
      SetOfAnything(factorSet, gcd)   
    }
  )
};

//////////////////////////////////////////////////////////////////////////////
// Extended Euclidean algorithm
// ax + by = gcd (a, b) -> Calcula x e y 
// Suponemos a y b sin factores comunes: gcd(a, b) = 1
Set PolExtendedGCD(Polyn a_aux, Polyn b_aux)
//////////////////////////////////////////////////////////////////////////////
{
  Real aux = True;

  Polyn x = 0;
  Polyn lastx = 1;
  
  Polyn y = 1;
  Polyn lasty = 0;

  Polyn a = Copy(a_aux);
  Polyn b = Copy(b_aux);

  Polyn While(Not(b == Polyn 0), {
    Set division = PolDivision(a, b);
    If(EQ(Card(division), 0), {
      WriteLn ("Error en la división de polinomios ", "E");
      WriteLn ("No es posible calcular los factores x e y tal que 
      ax+by =gcd (a, b) ", "E");
      Real aux := False;
      Polyn b := 0
    }, {
      Polyn q = division[1];
      Polyn r = division[2];

      Polyn a := b;
      Polyn b := r;

      Polyn upx = lastx - q*x;
      Polyn lastx := x;
      Polyn x := upx;
    
      Polyn upy = lasty - q*y;
      Polyn lasty := y;
      Polyn y := upy;
      y
    })
  }); 
  If(aux, {
    Set factorSet = If(NE(Degree(a),0), 
      SetOfPolyn(lastx, lasty),
      SetOfPolyn(Quotient(lastx/a), Quotient(lasty/a)) 
    );
    Polyn gcd = If(EQ(Degree(a),0), 1, a);
    SetOfAnything(factorSet, gcd)
  }, Empty
  )
};

//////////////////////////////////////////////////////////////////////////////
Real Test(Set setPol, Set factorSet)
//////////////////////////////////////////////////////////////////////////////
{
  If(Card(setPol)==Card(factorSet[1]),{
    If(factorSet[2]==Polyn 1, {
      Polyn aux = SetSum(For(1, Card(setPol), Polyn (Real n) {
        factorSet[1][n]*SetProd(setPol - [[ setPol[n] ]]) }));
        Set coef0 = Unique(For (1, Degree(aux), Real (Real i) {
      Coef(aux, i)>0.000001})); 
      Real If (Card(coef0), 
    If(Card(coef0)==1 & coef0[1]==0 & Round(Coef(aux,0),5)==1,
     True, False))}, {WriteLn( "Factores comunes", "E"); False})
  }, {WriteLn ("Argumentos erroneos en la función Test1", "E"); False}
  )
};

/*
//////////////////////////////////////////////////////////////////////////////
// n=0, cos 0 = 1;
// n=1, cos w = x;
// cosnw =2*cos(n-1)w*cosw - cos(n-2)w
//////////////////////////////////////////////////////////////////////////////
Polyn Cosnx(Real n)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn Case(n==0, 1,
             n==1, B,
             n>1, 2*Cosnx(n-1)*B-Cosnx(n-2)
  )
};
*/
//////////////////////////////////////////////////////////////////////////////
// cosnw =2*cos(n-1)w*cosw - cos(n-2)w
Polyn Cosnx(Real n)
//////////////////////////////////////////////////////////////////////////////
{
  Real k      = 2;
  Polyn cosn_1 = B;
  Polyn cosn_2 = 1;
  Polyn cosn   = If(EQ(n,1), B, 1); 

  Real While(LE(k, n), {
    Polyn cosn := 2*cosn_1*B -cosn_2;
    Polyn cosn_2 := cosn_1;
    Polyn cosn_1 := cosn;
    Real k := k+1
  });
  cosn
};

//////////////////////////////////////////////////////////////////////////////
// Transforma el polinomio simétrico en BF y lo pone en función de x
Polyn ChangeSymBFCosx(Polyn pol)
//////////////////////////////////////////////////////////////////////////////
{
  Set coefSet = For(0, Degree(pol), Polyn(Real d) {
    Cosnx(d)*(Real Coef(pol, d)*If(EQ(d,0), 1, 2))
  });
  SetSum(coefSet)
};

//////////////////////////////////////////////////////////////////////////////
// Transforma el polinomio en x y lo transforma en el simétrico BF
Polyn ChangeCosxSymBF(Polyn pol)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn symBF = ChangeB(pol, F/2+B/2);
  symBF
};

//////////////////////////////////////////////////////////////////////////////
Set GetPolynFromPolynBF(Polyn cBF)
//////////////////////////////////////////////////////////////////////////////
{
[[
  Matrix fun(Matrix x)
  {
    Polyn polIni = MatPol(x);
    Polyn pol = If(IsStationary(polIni),
      polIni,
      InverseNonStationaryRoots(polIni)
    );
    Polyn pol2 = pol*ChangeBF(pol);
    PolMat((B^Degree(cBF))*(cBF-pol2), 2*(Degree(cBF)+1)-1, 1)
  };
  
  
  Matrix iniMat = Rand(1, Degree(cBF)+1, 0.1, 0.1);
  Real n = Degree(cBF)+1;
  Real m = 2*(Degree(cBF)+1)-1;
  
  Real oldTolerance         = Copy(Tolerance);
  Real oldRelativeTolerance = Copy(RelativeTolerance);
  Real oldMaxIter           = Copy(MaxIter); 
  Real Tolerance := 5*(10^(-12));
  Real RelativeTolerance := 10^(-12);
  Real MaxIter := 1000;

  Set marqSet = Marquardt(n, m, fun, iniMat);

  Polyn tethaIni = MatPol(marqSet[1]);
  Polyn tetha = If(IsStationary(tethaIni),
    tethaIni,
    InverseNonStationaryRoots(tethaIni)
  ) 
]]
};

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

/*
// Forma recursiva: cuando n es grande, es muy lento
//////////////////////////////////////////////////////////////////////////////
// Cambio de variable: x^n=(B+F)^n 
// Ej: x^3 =(B+F)^3= B^3+F^3+3x -> B^3+F^3=x^3-3x
Polyn CV_(Real n)
//////////////////////////////////////////////////////////////////////////////
{
  Case(n==0, 1,
       n==1, B,
       n>1, {
         Polyn aux = (B+F)^n;
         Polyn aux2 = SetSum(For(0, Degree(aux)-1, Polyn (Real i) {
           Coef(aux, i)*CV(i)
         }));
         B^n-aux2
       })
};
*/

//////////////////////////////////////////////////////////////////////////////
Polyn CV(Real n)
//////////////////////////////////////////////////////////////////////////////
{
  Case(n==0, 1, 
       n==1, B, 
       n>1, {
  Polyn result = B^n;
  Polyn auxM = (B+F)^n;
  Set For(1, n/2, Polyn (Real i){
    Real d = n-2*i;
    Real coef = (-1)*Coef(auxM, d);
    Polyn result := result + coef*B^d;
    Polyn auxM := auxM + coef*(B+F)^d
  }); 
  result
  })
};

// Division de dos polinomios simetricos
// Los argumentos num y den no son polinomios simétricos
//////////////////////////////////////////////////////////////////////////////
Set SymPolDivision(Polyn num, Polyn den)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn num_S = num*ChangeBF(num);
  Polyn den_S = den*ChangeBF(den);

  If(Degree(num_S)< Degree(den_S), {
    Polyn q_S = 1;
    Polyn r_S = num_S;
    [[q_S, r_S]]
  }, { 
    Polyn num_T = SetSum(For(0, Degree(num_S), Polyn (Real j){
      Coef(num_S, j)*CV(j)
    }));
    Polyn den_T = SetSum(For(0, Degree(den_S), Polyn (Real j){
      Coef(den_S, j)*CV(j)
    }));
    Set div_T = PolDivision(num_T, den_T);
    Polyn q_T = div_T[1];
    Polyn r_T = div_T[2];
  
    Polyn q_S = ChangeB(q_T, B+F);
    Polyn r_S = ChangeB(r_T, B+F);
    [[q_S, r_S]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Set SymPolDivision2(Polyn num_S, Polyn den_S)
//////////////////////////////////////////////////////////////////////////////
{
 
  Polyn num_T = SetSum(For(0, Degree(num_S), Polyn (Real j){
    Coef(num_S, j)*CV(j)
  }));
  Polyn den_T = SetSum(For(0, Degree(den_S), Polyn (Real j){
    Coef(den_S, j)*CV(j)
  }));
  Set div_T = PolDivision(num_T, den_T);
  Polyn q_T = div_T[1];
  Polyn r_T = div_T[2];
  
  Polyn q_S = ChangeB(q_T, B+F);
  Polyn r_S = ChangeB(r_T, B+F);
  [[q_S, r_S]]
};

